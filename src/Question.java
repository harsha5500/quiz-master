package QuizMaster;

class Question {

    public void Question() {
        questionID = 0;
        statement = "";
        correctOption = 0;
        marks = 0;
        selectedOption = 0;
//        options = new String[4];
//        for (int i = 0; i < 4; i++) {
//            options[i] = "";
//        }
    }

    public int getQuestionID() {
        return questionID;
    }

    public int getCorrectOption() {
        return correctOption;
    }

    public String getStatement() {
        return statement;
    }

    public int getMarks() {
        return marks;
    }

    public int getSelectedOption() {
        return selectedOption;
    }

    public String[] getOptions() {
        //String[] temp = new String[4];
        return options;
    }

    public void setQuestionID(int id) {
        questionID = id;
    }

    public void setCorrectOption(int option) {
        correctOption = option;
    }

    public void setStatement(String stmt) {
        statement = stmt;
    }

    public void setMarks(int m) {
        marks = m;
    }

    public void setSelectedOption(int option) {
        selectedOption = option;
    }

    public void setOptions(String one, String two, String three, String four) {
        options = new String[4];
        if (one == null) {
            one = "";
        }
        if (two == null) {
            two = "";
        }
        if (three == null) {
            three = "";
        }
        if (four == null) {
            four = "";
        }
        options[0] = "" + one;
        options[1] = "" + two;
        options[2] = "" + three;
        options[3] = "" + four;
    }
//    private String option1;
//    private String option2;
//    private String option3;
//    private String option4;
    private String[] options;
    private int questionID;
    private String statement;
    private int correctOption;
    private int marks;
    private int selectedOption;
}
