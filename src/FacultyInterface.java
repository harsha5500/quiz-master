/*
 * FacultyInterface.java
 *
 * Created on December 17, 2007, 10:28 PM
 */
package QuizMaster;

import java.sql.SQLException;
//import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author  DeadEyes
 */
public class FacultyInterface extends javax.swing.JFrame {

    /** Creates new form FacultyInterface */
    public FacultyInterface() {
        initComponents();
        pendingModel = new DefaultListModel();
        performanceModel = new DefaultListModel();
        jList1.setModel(pendingModel);
        jList2.setModel(performanceModel);
    }

    public void setInitialState(String fname) {
        DBAccess question = new DBAccess();
        try {
            question.Query = "select COUNT(questionID) from question where faculty ='" + fname + "';";
            java.sql.ResultSet myresult = question.ExecQuery();
            if (myresult.next()) {

                //set no of questions in database
                String noOfQuestions = myresult.getString(1);
                jTextField1.setText(noOfQuestions);

                //set the pending test list
                question.Query = "select paperID from questionpaper where status = 'pending' and paperID in(select paperID from questionset where questionID in (select questionID from question where faculty='" + fname + "'));";
                myresult = question.ExecQuery();
                for (int i = 0; myresult.next(); i++) {
                    pendingModel.add(i, myresult.getString(1));
                }

                //set the completed list
                question.Query = "select paperID from questionpaper where status = 'complete' and paperID in(select paperID from questionset where questionID in (select questionID from question where faculty='" + fname + "'));";
                myresult = question.ExecQuery();
                for (int i = 0; myresult.next(); i++) {
                    performanceModel.add(i, myresult.getString(1));
                }
            }
            question.CloseConnection();
            return;
        } catch (SQLException ex) {
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            question.CloseConnection();
        }
    }

    public void setFacultyName(String fname) {
        name = fname;
    }

    public String getFacultyName() {
        return name;
    }

    public int addToPendingList(int Id) {
        DBAccess MyDatabase = new DBAccess();
        DBAccess MyUpdate = new DBAccess();
        try {
            MyDatabase.Query = "select username from users where type = 'student';";
            java.sql.ResultSet resultSet = MyDatabase.ExecQuery();
            while (resultSet.next()) {
                String uname = resultSet.getString(1);
                MyUpdate.Update = "insert into pendinglist values(" + Id + ",'" + uname + "');";
                if (MyUpdate.ExecUpdate() == null) {
                    JOptionPane.showMessageDialog(this, "Invalid condiation. Unable to update the pending list.");
                    return -1;
                }
            }
            MyDatabase.CloseConnection();
            MyUpdate.CloseConnection();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(this, "Invalid condiation. Unable to read the user list.");
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            MyDatabase.CloseConnection();
            MyUpdate.CloseConnection();
        }
        return 1;
    }

    public int addQuestions(String facultyName, int maxMarks, int testId) {
        DBAccess MyDatabase = new DBAccess();
        DBAccess MyUpdate = new DBAccess();
        try {
            //check if the database has enough questions for the test
            MyDatabase.Query = "select SUM(marks) from question where faculty = '" + facultyName + "';";
            java.sql.ResultSet resultSet = MyDatabase.ExecQuery();
            int totalMarks;

            if (resultSet.next()) //if result exsists
            {
                totalMarks = resultSet.getInt(1);

            } else {
                //call the apprioate interface
                JOptionPane.showMessageDialog(this, "Invalid condiation. Unable to read marks.");
                return -1;
            }

            if (totalMarks < maxMarks) {
                JOptionPane.showMessageDialog(this, "Not enough questions exsist to set the test now. Add more questions and try again.");
                return -1;
            } else {
                //Enough questions exsist.Start adding them to the test database

                int marksCounter = 0;
//                int questionCount = 0;
//
//                MyDatabase.Query = "select COUNT(questionID) from question where faculty = '" + facultyName + "';";
//                resultSet = MyDatabase.ExecQuery();
//                if (resultSet.next()) //if result exsists
//                {
//                    questionCount = resultSet.getInt(1);
//                } else {
//                    //call the apprioate interface
//                    JOptionPane.showMessageDialog(this, "Invalid condiation. Unable to read count.");
//                    return -1;
//                }
//
                //set up a random number counter
//                Random generator = new Random();
//                int randomNumber = 0;
//                for (int i = 0; (i < questionCount) && (marksCounter <= maxMarks); i++) {
//                    randomNumber = generator.nextInt(questionCount);
//                    MyDatabase.Query = "select questionID from question where questionID = '" + randomNumber + "';";
//                    resultSet = MyDatabase.ExecQuery();
//
//                    if (resultSet.next()) //if result exsists
//                    {
//                        MyDatabase.Update = "insert into questionset values ('" + testId + "','" + resultSet.getString(1) + "';";
//                        if (MyDatabase.ExecUpdate() == null) {
//                            JOptionPane.showMessageDialog(this, "Invalid condiation. Unable to insert into questionset");
//                            return -1;
//                        }
//                        marksCounter += resultSet.getInt(2);
//
//                    } else {
//                        //call the apprioate interface
//                        JOptionPane.showMessageDialog(this, "Invalid condiation. A question was not found.");
//                        return -1;
//                    }
//
//                }
                MyDatabase.Query = "select questionID,marks from question where faculty = '" + facultyName + "';";
                resultSet = MyDatabase.ExecQuery();

                while (resultSet.next() && (marksCounter <= maxMarks)) {
                    MyUpdate.Update = "insert into questionset values (" + testId + "," + resultSet.getInt(1) + ");";
                    if (MyUpdate.ExecUpdate() == null) {
                        JOptionPane.showMessageDialog(this, "Invalid condiation. Unable to insert into questionset");
                        return -1;
                    }
                    marksCounter += resultSet.getInt(2);
                }
                //done adding all questions return 
                MyUpdate.CloseConnection();
                MyDatabase.CloseConnection();
                return 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            MyDatabase.CloseConnection();
            MyUpdate.CloseConnection();
        }
        //code would never come here. Added to compile the code.
        return -1;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jLabel3 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jLabel4 = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Quiz Master - Faculty");
        setBounds(new java.awt.Rectangle(200, 200, 600, 525));
        setMinimumSize(new java.awt.Dimension(630, 525));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Quiz Master");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Faculty");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 50, -1, -1));

        jButton1.setText("Add ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 390, 110, 30));

        jButton2.setText("Remove");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 430, 110, 30));

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Tahoma", 1, 14));
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 340, 120, 30));

        jList1.setBackground(java.awt.SystemColor.control);
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jList1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 130, 90));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Pending Tests");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, 100, 20));

        jButton4.setText("Remove Test");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 220, 130, -1));

        jButton5.setText("Check Status");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, 130, -1));

        jList2.setBackground(java.awt.SystemColor.control);
        jList2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jList2);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 90, 130, 110));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Completed Tests");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 60, -1, -1));

        jButton6.setText(" Performance");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 220, 120, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Questions");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 310, 80, 20));

        jTextArea1.setBackground(java.awt.SystemColor.control);
        jTextArea1.setColumns(20);
        jTextArea1.setEditable(false);
        jTextArea1.setRows(5);
        jScrollPane3.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, 240, 190));
        getContentPane().add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 350, 50, -1));
        getContentPane().add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 380, 50, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Max Marks:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 350, -1, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Duration:");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 380, -1, -1));

        jLabel8.setText("(in minutes)");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 400, -1, -1));

        jButton3.setText("Set Test");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 430, 90, 30));

        jSeparator1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, 150, 190));

        jSeparator2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 310, 150, 160));

        jSeparator3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 150, 210));

        jSeparator4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        getContentPane().add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 80, 260, 210));

        jSeparator5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 50, 150, 210));

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Prepare Test");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 290, -1, -1));

        jButton7.setText("Logout");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 440, -1, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Test ID:");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, -1, -1));

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField4, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 320, 50, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        DBAccess Mydatabase = new DBAccess();
        DBAccess Myupdate = new DBAccess();
        DBAccess Myupdate2 = new DBAccess();
        java.sql.ResultSet resultSet, updateResult;

        String Id = JOptionPane.showInputDialog("Enter the Id or the question to be removed:");
        int qid;

        try {
            qid = Integer.parseInt(Id);

            //Check if the question exsists
            Mydatabase.Query = "select * from question where questionID=" + Id + ";";
            resultSet = Mydatabase.ExecQuery();
            if (resultSet.next() == false) {
                //No such question
                JOptionPane.showMessageDialog(null, " Question does not exsist.");
                return;
            }

            //check if any pending papers have the question listed
            Mydatabase.Query = "select paperID from questionset where questionID=" + Id + ";";
            resultSet = Mydatabase.ExecQuery();
            if (resultSet.next()) {
                //check if it is a pending paper
                int paperId = resultSet.getInt(1);
                Myupdate.Query = "select paperID from questionpaper where paperID=" + paperId + "and status='pending';";
                updateResult = Mydatabase.ExecQuery();
                if (updateResult.next()) {
                    JOptionPane.showMessageDialog(null, "A current pending question paper has listed this question. Please remove the test or wait till the paper is completed");
                    return;
                }
            }

            //delete the options
            Mydatabase.Update = "delete from options where questionID=" + qid + ";";
            if (Mydatabase.ExecUpdate() == null) {
                JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
            } else {
                //delete from question
                Myupdate2.Update = "delete from question where questionID=" + qid + ";";
                if (Myupdate2.ExecUpdate() == null) {
                    JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
                } else {


                    Myupdate.Query = "select COUNT(questionID) from question where faculty ='" + getFacultyName() + "';";
                    updateResult = Myupdate.ExecQuery();
                    if (updateResult.next()) {
                        //set no of questions in database
                        String noOfQuestions = updateResult.getString(1);
                        jTextField1.setText(noOfQuestions);
                        JOptionPane.showMessageDialog(null, "Question Removed.");

                        //close the database connection
                        Mydatabase.CloseConnection();
                        Myupdate.CloseConnection();
                        Myupdate2.CloseConnection();
                        return;
                    }
                }
            }
        } catch (NumberFormatException e) {
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, " Please Enter a valid Id");
            e.printStackTrace();
        } catch (SQLException ex) {

        } finally {
            Mydatabase.CloseConnection();
            Myupdate.CloseConnection();
            Myupdate2.CloseConnection();
        }
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        AddQuestionInterface temp = new AddQuestionInterface();
        temp.setVisible(true);
        temp.setFacultyName(getFacultyName());
        this.setVisible(false);
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
        new Login().setVisible(true);
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        //Validations

        String id = jTextField4.getText();
        String marks = jTextField2.getText();
        String duration = jTextField3.getText();
        int maxMarks = -1;
        int testDuration = -1;
        int testId = -1;
        DBAccess MyDatabase = new DBAccess();

        try {
            maxMarks = Integer.parseInt(marks);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Invalid entry for marks");
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
            return;
        }

        try {
            testDuration = Integer.parseInt(duration);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Invalid entry for duration");
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
            return;
        }

        try {
            testId = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Invalid entry for duration");
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
            return;
        }

        if (id.equals("")) {
            JOptionPane.showMessageDialog(this, "Please enter a valid testId (unsigned  int).");
        } else if (marks.equals("")) {
            JOptionPane.showMessageDialog(this, "Please enter the total marks for the test.");
        } else if (duration.equals("")) {
            JOptionPane.showMessageDialog(this, "Please enter the time limit for the test in minutes.");
        } else if (maxMarks <= 0 || testDuration <= 0 || testId < 0) {
            JOptionPane.showMessageDialog(this, "Invalid entry,Check the entries for duration and marks or testId");
            jTextField2.setText("");
            jTextField3.setText("");
            jTextField4.setText("");
        } else {

            // All ok set the test
            try {

                //Adding a test
                MyDatabase.Update = "insert into questionpaper values(" + testId + "," + maxMarks + "," + testDuration + "," + "'pending');";

                // process query results
                if (MyDatabase.ExecUpdate() == null) //if test added successfully
                {
                    JOptionPane.showMessageDialog(null, "Error setting test.");
                } else {
                    //Adding a set of randomly selected questions from a faculty's set of questions.
                    if (addQuestions(getFacultyName(), maxMarks, testId) == 1) {
                        //add the test to pending list of the students
                        if (addToPendingList(testId) == 1) {
                            //add it to pending list of current form.

//                            DefaultListModel model = new DefaultListModel();
//                            jList1.setModel(model);
                            int pos = jList1.getModel().getSize();
                            String data = "" + testId;
                            pendingModel.add(pos, data);

                        //jList1.setListData(data);
                        } else {
                            //roll back questionpaper
                            MyDatabase.Update = "delete from questionpaper where paperID =" + testId + ";";
                            if (MyDatabase.ExecUpdate() == null) //if test added successfully
                            {
                                JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
                            }
                        }
                    } else {
                        //roll back questionpaper
                        MyDatabase.Update = "delete from questionpaper where paperID =" + testId + ";";
                        if (MyDatabase.ExecUpdate() == null) //if test added successfully
                        {
                            JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
                        }
                    }

                    MyDatabase.CloseConnection();
                }
            } finally {
                MyDatabase.CloseConnection();
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
    // TODO add your handling code here:
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        DBAccess Mydatabase = new DBAccess();
        int Id;
        String message = "";
        String selection = (String) jList1.getSelectedValue();
        Id = Integer.parseInt(selection);
        try {
            //check the max marks and time limit.
            Mydatabase.Query = "select maxmarks,duration from questionpaper where paperID=" + Id + ";";
            java.sql.ResultSet resultSet = Mydatabase.ExecQuery();
            if (resultSet.next()) {
                //jTextArea1.setText("TestId:"+Id+"\nMaximum Marks:"+resultSet.getString(1)+"\nDuration:"+resultSet.getString(2));  
                message = "TestId:" + Id + "\nMaximum Marks:" + resultSet.getString(1) + "\nDuration:" + resultSet.getString(2);
            }

            //check no of students pending
            Mydatabase.Query = "select COUNT(student) from pendinglist where paperID=" + Id + ";";
            resultSet = Mydatabase.ExecQuery();
            if (resultSet.next()) {

                jTextArea1.setText(message + "\nNo of pending students:" + resultSet.getString(1));
            }
            Mydatabase.CloseConnection();
            return;
        } catch (SQLException ex) {
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Mydatabase.CloseConnection();
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        DBAccess Mydatabase = new DBAccess();
        String selection = (String) jList1.getSelectedValue();
        int Id = Integer.parseInt(selection);

        //pendinglist
        Mydatabase.Update = "delete from pendinglist where paperID =" + Id + ";";
        if (Mydatabase.ExecUpdate() == null) //if test added successfully
        {
            JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
            return;
        } else {

            //questionset
            Mydatabase.Update = "delete from questionset where paperID =" + Id + ";";
            if (Mydatabase.ExecUpdate() == null) //if test added successfully
            {
                JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
                return;
            } else {
                //questionpaper
                Mydatabase.Update = "delete from questionpaper where paperID =" + Id + ";";
                if (Mydatabase.ExecUpdate() == null) //if test added successfully
                {
                    JOptionPane.showMessageDialog(null, "Error possible compamise of database.");
                    return;
                } else {
                    //update the JList
                    int select = jList1.getSelectedIndex();
                    pendingModel.remove(select);
                    return;
                }
            }
        }

    }//GEN-LAST:event_jButton4ActionPerformed

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
    // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        String selection = (String) jList2.getSelectedValue();
        DBAccess Mydatabase = new DBAccess();
        DBAccess Mydatabase2 = new DBAccess();
        if (selection == null) {
            return;
        }
        int Id = Integer.parseInt(selection);

        //calculate the average of all students whith the test id
        try {
            Mydatabase.Query = "select AVG(permarks) from student where paperID = " + Id + ";";
            java.sql.ResultSet resultSet = Mydatabase.ExecQuery();
            if (resultSet.next()) {
                Mydatabase2.Query = "select MAX(permarks) from student where paperID = " + Id + ";";
                java.sql.ResultSet maxResult = Mydatabase2.ExecQuery();
                if (maxResult.next()) {
                    String message = "PaperID: " + Id + "\nAverage Marks: " + resultSet.getFloat(1) + "\nMaxScore: " + maxResult.getInt(1);
                    jTextArea1.setText(message);
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Mydatabase.CloseConnection();
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new FacultyInterface().setVisible(true);
            }
        });
    }
    private String name;
    private DefaultListModel performanceModel;
    private DefaultListModel pendingModel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList jList1;
    private javax.swing.JList jList2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables
}
