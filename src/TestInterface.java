/*
 * TestInterface.java
 *
 * Created on December 19, 2007, 1:16 PM
 */
package QuizMaster;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

/**
 *
 * @author  DeadEyes
 */
public class TestInterface extends javax.swing.JFrame {

    /** Creates new form TestInterface */
    public TestInterface() {
        initComponents();
        noOfQuestionsAnswered = 0;
        testQuestions = new ArrayList<Question>();
        optionModel = new DefaultListModel();
        time = new Timer(1000, jTextField1.getActionListeners()[0]);
    }

    public void setStudentName(String fname) {
        studentName = fname;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setPaperID(int id) {
        paperId = id;
    }

    public int getPaperID() {
        return paperId;
    }

    public void overTimeSubmit() {
        try {
            // TODO add your handling code here:
            int score = 0;
            int totalMarks = 0;
            float perScore = 0.0f;

            DBAccess studentResult = new DBAccess();

            //calculate test score
            for (int i = 0; i < testQuestions.size(); i++) {
                totalMarks += testQuestions.get(i).getMarks();
                if (testQuestions.get(i).getSelectedOption() > 0) {
                    if (testQuestions.get(i).getSelectedOption() == testQuestions.get(i).getCorrectOption()) {
                        score += testQuestions.get(i).getMarks();
                    }
                //else he has answered wrongly
                }
                System.out.println("here:" + "\nSize" + testQuestions.size() + "\nTotalMarks" + totalMarks + "\nscore" + score);
            //else he has answered wrongly
            }


            //Add the score to student's marks database.
            perScore = ((float)score / (float)totalMarks) * 100;
            studentResult.Update = "insert into student values ('" + getStudentName() + "'," + getPaperID() + "," + perScore + ")";
            if (studentResult.ExecUpdate() == null) {
                JOptionPane.showMessageDialog(null, "error Inserting student data");
                return;
            }
            //remove him from pending list
            studentResult.Update = "delete from pendinglist where student = '" + getStudentName() + "';";
            if (studentResult.ExecUpdate() == null) {
                JOptionPane.showMessageDialog(null, "error Inserting student data");
                return;
            }

            studentResult.CloseConnection();

            //check situation of the test itself to see if all have completed it.
            DBAccess checkTestComplete = new DBAccess();
            java.sql.ResultSet resultSet;

            checkTestComplete.Query = "select student from pendiglist where paperID=" + getPaperID() + ";";

            resultSet = checkTestComplete.ExecQuery();

            if (resultSet.next()) {
            //The test is still not complete

            } else {

                //else it is complete hence update the same.
                checkTestComplete.Update = "update questionpaper set status = 'complete' where paperID =" + getPaperID() + ";";
                if (checkTestComplete == null) {
                    JOptionPane.showMessageDialog(null, "error Inserting student data");
                    return;
                }
            }

            //restart student interface
            StudentInterface temp = new StudentInterface();
            temp.setStudentName(getStudentName());
            temp.setInitialState(getStudentName());
            temp.setVisible(true);
            this.setVisible(false);
            dispose();

        /*NOTE The students overall score may be updated however a student cannot view the performance
         * of the test untill all students have completed the test.
         * Also note the update to student's average is not done twice but only once as it read from the database.
         * */
        } catch (SQLException ex) {
            Logger.getLogger(TestInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setInitialState(String name, int id) {
        DBAccess Mydatabase = new DBAccess();
        setStudentName(name);
        setPaperID(id);

        //Set the duration of the test
        try {
            Mydatabase.Query = "select duration from questionpaper where paperID=" + id + ";";
            java.sql.ResultSet resultSet = Mydatabase.ExecQuery();
            if (resultSet.next()) {
                durationMinutes = resultSet.getInt(1);
                jTextField1.setText(durationMinutes + " : " + durationSeconds);

            }
            //Select the number of questions present in the test.
            Mydatabase.Query = "select COUNT(questionID) from questionset where paperID=" + id + ";";
            resultSet = Mydatabase.ExecQuery();
            if (resultSet.next()) {
                noOfQuestions = resultSet.getInt(1);
                String questionDisplay = noOfQuestionsAnswered + " | " + noOfQuestions;
                jTextField2.setText(questionDisplay);
            }


            //copy all the questionset into a arraylist
            int qid = 0;
            DBAccess Myquestion = new DBAccess();
            DBAccess Myoption = new DBAccess();
            java.sql.ResultSet questionSet, optionSet;

            Mydatabase.Query = "select questionID from questionset where paperID=" + id + ";";
            resultSet = Mydatabase.ExecQuery();

            while (resultSet.next()) {
                qid = resultSet.getInt(1);

                Myquestion.Query = "select marks,question_statement from  question where questionID = " + qid + ";";
                Myoption.Query = "select option1,option2,option3,option4,correct from options where questionID=" + qid + ";";
                questionSet = Myquestion.ExecQuery();
                optionSet = Myoption.ExecQuery();

                if (questionSet.next() && optionSet.next()) {
                    Question myQuestion = new Question();

                    //initilize everything
                    myQuestion.setQuestionID(qid);
                    myQuestion.setMarks(questionSet.getInt(1));
                    myQuestion.setStatement(questionSet.getString(2));

                    //
                    //System.out.println(optionSet.getString(1)+","+ optionSet.getString(2)+","+ optionSet.getString(3)+","+ optionSet.getString(4));
                    //                 

                    myQuestion.setOptions(optionSet.getString(1), optionSet.getString(2), optionSet.getString(3), optionSet.getString(4));
                    myQuestion.setCorrectOption(optionSet.getInt(5));
                    testQuestions.add(myQuestion);
                }

            }

            //add the indices of the questions to the combox
            for (int i = 0; i < testQuestions.size(); i++) {
                jComboBox1.addItem(i + 1);
            }
            Mydatabase.CloseConnection();
        } catch (SQLException ex) {
            Logger.getLogger(FacultyInterface.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            Mydatabase.CloseConnection();
        }

    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jTextField1 = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jTextField2 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Quiz Master - Test");
        setBounds(new java.awt.Rectangle(100, 100, 580, 575));
        setMinimumSize(new java.awt.Dimension(580, 575));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Quiz Master");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 20, 150, 30));

        jTextArea1.setBackground(java.awt.SystemColor.control);
        jTextArea1.setColumns(20);
        jTextArea1.setEditable(false);
        jTextArea1.setRows(5);
        jTextArea1.setEnabled(false);
        jScrollPane1.setViewportView(jTextArea1);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 280, 310, 110));

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance().getContext().getResourceMap(TestInterface.class);
        jTextField1.setBackground(resourceMap.getColor("jTextField1.background")); // NOI18N
        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Tahoma", 1, 24));
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 170, 40));

        jComboBox1.setEnabled(false);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        getContentPane().add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 250, 70, -1));

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton1.setText(">");
        jButton1.setEnabled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 280, 90, 110));

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton2.setText("<"); // NOI18N
        jButton2.setEnabled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 280, 90, 110));

        jTextField2.setEditable(false);
        jTextField2.setFont(new java.awt.Font("Tahoma", 1, 24));
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        getContentPane().add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 180, 170, 40));

        jButton3.setText("Start Test!");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 140, 110, 40));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Test");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 60, 30, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Time Remaining");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 110, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Question:");
        jLabel4.setEnabled(false);
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 250, -1, -1));

        jButton4.setText("Submit");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, 110, 40));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Answer (Select the right option and click next)");
        jLabel5.setEnabled(false);
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 400, 320, 40));

        jList1.setBackground(java.awt.SystemColor.control);
        jList1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList1.setEnabled(false);
        jScrollPane2.setViewportView(jList1);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 430, 280, 90));

        jSeparator1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 100, 190, 130));

        jSeparator2.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 550, 290));

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
        if (durationSeconds == 0) {
            durationMinutes--;
            durationSeconds = 59;
        } else {
            durationSeconds--;
        }
        jTextField1.setText(durationMinutes + " : " + durationSeconds);

        if (durationMinutes == 0 && durationSeconds == 0) {
            //YOU HAVE RUN OUT OF TIME
            JOptionPane.showMessageDialog(null, "You have run out of time. Test is automatically submitted");
            overTimeSubmit();
            time.stop();
            jTextField1.setText("0 : 0");

            //disable all controls for test
            jSeparator2.setEnabled(false);
            jLabel4.setEnabled(false);
            jComboBox1.setEnabled(false);
            jButton1.setEnabled(false);
            jButton2.setEnabled(false);
            jTextArea1.setEnabled(false);
            jLabel5.setEnabled(false);
            jList1.setEnabled(false);
        }
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:

        //Enable all the components
        jSeparator2.setEnabled(true);
        jLabel4.setEnabled(true);
        jComboBox1.setEnabled(true);
        jButton1.setEnabled(true);
        jButton2.setEnabled(true);
        jTextArea1.setEnabled(true);
        jLabel5.setEnabled(true);
        jList1.setEnabled(true);
        //disable reset :)
        jButton3.setEnabled(false);
        //start timer
        time.start();

        displayQuestion(0);
        
        
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        displayQuestion(jComboBox1.getSelectedIndex());
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int selection = jList1.getSelectedIndex();//answer

        //System.out.println(selection);
        int question = jComboBox1.getSelectedIndex();//question

        //update current question
        Question temp = testQuestions.get(question);
        if (selection != -1) {
            if (temp.getSelectedOption() == 0)//fresh selection
            {
                noOfQuestionsAnswered++;
            }
            temp.setSelectedOption(selection + 1);//stale selection
            jTextField2.setText(noOfQuestionsAnswered + " | " + noOfQuestions);
        }
        //show next question
        if (question == (jComboBox1.getItemCount() - 1)) {
            return;
        } else {
            jComboBox1.setSelectedIndex(question + 1);
            displayQuestion(question + 1);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        int selection = jList1.getSelectedIndex();
        //System.out.println(selection);
        int question = jComboBox1.getSelectedIndex();

        //update current question
        Question temp = testQuestions.get(question);
        if (selection > 0) {
            if (temp.getSelectedOption() == 0)//fresh selection
            {
                noOfQuestionsAnswered++;
            }
            temp.setSelectedOption(selection + 1);//stale selection
            jTextField2.setText(noOfQuestionsAnswered + " | " + noOfQuestions);
        }
        //show next question
        if (question == 0) {
            return;
        } else {
            jComboBox1.setSelectedIndex(question - 1);
            displayQuestion(question - 1);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try {
            // TODO add your handling code here:
            int score = 0;
            int totalMarks = 0;
            float perScore = 0.0f;

            DBAccess studentResult = new DBAccess();

            //calculate test score
            for (int i = 0; i < testQuestions.size(); i++) {
                totalMarks += testQuestions.get(i).getMarks();
                if (testQuestions.get(i).getSelectedOption() > 0) {
                    if (testQuestions.get(i).getSelectedOption() == testQuestions.get(i).getCorrectOption()) {
                        score += testQuestions.get(i).getMarks();
                    }
                //else he has answered wrongly
                }
            //else he has answered wrongly
            }


            //Add the score to student's marks database.
            perScore = (score / totalMarks) * 100;

            studentResult.Update = "insert into student values ('" + getStudentName() + "'," + getPaperID() + "," + perScore + ")";
            if (studentResult.ExecUpdate() == null) {
                JOptionPane.showMessageDialog(null, "error Inserting student data");
                return;
            }

            //
            System.out.println("student insert successful");
            //

            //remove him from pending list
            studentResult.Update = "delete from pendinglist where student = '" + getStudentName() + "';";
            if (studentResult.ExecUpdate() == null) {
                JOptionPane.showMessageDialog(null, "error deleting pending data");
                return;
            }


            //
            System.out.println("delete from pending successful");
            //

            studentResult.CloseConnection();

            //check situation of the test itself to see if all have completed it.
            DBAccess checkTestComplete = new DBAccess();
            java.sql.ResultSet resultSet;

            checkTestComplete.Query = "select student from pendinglist where paperID=" + getPaperID() + ";";

            resultSet = checkTestComplete.ExecQuery();

            if (resultSet.next()) {
                //The test is still not complete

                //
                System.out.println("test still not complete");
                //

                checkTestComplete.CloseConnection();
                StudentInterface temp = new StudentInterface();
                temp.setStudentName(getStudentName());
                temp.setInitialState(getStudentName());
                temp.setVisible(true);
                this.setVisible(false);
                dispose();

            } else {

                //else it is complete hence update the same.
                checkTestComplete.Update = "update questionpaper set status = 'complete' where paperID =" + getPaperID() + ";";

                if (checkTestComplete.ExecUpdate() == null) {
                    JOptionPane.showMessageDialog(null, "error Inserting student data");
                    return;
                }
                //
                System.out.println("test complete ");
                //

                checkTestComplete.CloseConnection();
                StudentInterface temp = new StudentInterface();
                temp.setStudentName(getStudentName());
                temp.setInitialState(getStudentName());
                temp.setVisible(true);
                this.setVisible(false);
                dispose();

             //
                System.out.println("here:" + "\nSize" + testQuestions.size() + "\nTotalMarks" + totalMarks + "\nscore" + score);
            //
            }


        } catch (SQLException ex) {
            Logger.getLogger(TestInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }//GEN-LAST:event_jButton4ActionPerformed

    public void displayQuestion(int index) {
        Question temp = testQuestions.get(index);

        //add the statements
        jTextArea1.setText(temp.getStatement());

        //add the options    
        optionModel.clear();

        for (int i = 0; i < temp.getOptions().length; i++) {
            optionModel.add(i, temp.getOptions()[i]);
        }
        jList1.setModel(optionModel);

        //if already selected
        if (temp.getSelectedOption() > 0) {
            jList1.setSelectedIndex(temp.getSelectedOption() - 1);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TestInterface().setVisible(true);
            }
        });
    }
    private int paperId;
    private int durationMinutes;
    private int durationSeconds;
    private String studentName;
    private int noOfQuestions;
    private int noOfQuestionsAnswered;
    private ArrayList<Question> testQuestions;
    private DefaultListModel optionModel;
    Timer time;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
