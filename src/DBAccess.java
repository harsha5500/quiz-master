package QuizMaster;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DeadEyes
 */

import java.sql.*;

public class DBAccess {
//DBLogin info
    private String URL;
    private String DBUsername;
    private String DBPassword;
    //Querying
    public String Query;
    public String Update;
    private Statement stmt;
    private Connection con;
    private ResultSet result_query;
    private Integer result_update;
    private String ResultString;

    /** Creates a new instance of DBTest */
    public DBAccess() {
        //Set Values
        URL = "jdbc:mysql://localhost/quizmaster";
        DBUsername = "";
        DBPassword = "";
        stmt = null;
        con = null;

        LoadDriver();

        ConnectToDB();

    }

    private void LoadDriver() {

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            System.err.println("Failed to load Connector/J");
            System.out.println("Check MySql Connector/J... ");
            e.printStackTrace();
        }
    }

    private void ConnectToDB() {

        try {
            con = java.sql.DriverManager.getConnection(
                    URL,
                    DBUsername,
                    DBPassword);
            stmt = con.createStatement();
        } catch (Exception e) {
            System.err.println("Problems connecting to " + URL);
            System.out.println("Check if MySQL daemon is running...");
            e.printStackTrace();

        }

    }

//Custom defined query
    public ResultSet ExecQuery() {
        try {
            result_query = stmt.executeQuery(Query);
            //CloseConnection();
            return result_query;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        //CloseConnection();
        return null;
    }

    public Integer ExecUpdate() {
        try {
            result_update = stmt.executeUpdate(Update);
            //CloseConnection();
            return result_update;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        //CloseConnection();
        return null;
    }

    void CloseConnection() {
        //close connection once done
        try {
            con.close();
        } catch (Exception e) {
            System.err.println("Error closing connection to " + URL);
            System.out.println("Check if MySQL daemon is running..");
        }
    }
}
